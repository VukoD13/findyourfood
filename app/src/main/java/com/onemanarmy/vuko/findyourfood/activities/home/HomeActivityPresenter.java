package com.onemanarmy.vuko.findyourfood.activities.home;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.onemanarmy.vuko.findyourfood.activities.IActivityPresenter;
import com.onemanarmy.vuko.findyourfood.model.domain.Place;
import com.onemanarmy.vuko.findyourfood.model.service.PlaceService;
import com.onemanarmy.vuko.findyourfood.utility.CurrentDate;
import com.onemanarmy.vuko.findyourfood.utility.ErrorHandler;
import com.onemanarmy.vuko.findyourfood.utility.Tracker;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 *
 */
public class HomeActivityPresenter implements IActivityPresenter
{
    private HomeActivity view;
    private PlaceService placeService;

    private final String CLIENT_ID = "2KRSN0SEMJ2QLHWMCVLCQD5FLHWXJNBCANTYWLHNKCH25UAM";
    private final String SECRET_ID = "BZELMO1UW2BUFMJZQJA503GUHDNB3RNKMK24IOSBKTKK10WY";
    private final int PLACES_LIMIT = 13;
    private final int PHOTO_LIMIT = 2;

    private List<Place> places;

    public HomeActivityPresenter(HomeActivity view)
    {
        this.view = view;

        placeService = new PlaceService();

        places = new ArrayList<>();
    }

    private String getLLAsStringForUrl()
    {
        String ll = Tracker.getInstance().getLatitude() + "," + Tracker.getInstance().getLongitude();
        return ll;
    }

    public void recreateHomeActivity()
    {
        view.finish();
        Intent intent = new Intent(view.getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        view.startActivity(intent);
    }

    @Override
    public void onCreate()
    {
        Tracker.getInstance().setHomeActivityPresenter(this);

        view.setRecyclerView(places);

        loadPlaces();
    }

    private Subscriber<JsonElement> loadPlacesSubscriber = new Subscriber<JsonElement>()
    {
        @Override
        public void onStart()
        {
            view.showProgressDialog();
        }

        @Override
        public void onNext(JsonElement jsonElement)
        {
            JsonObject jsonObject = (JsonObject) jsonElement;
            jsonObject = jsonObject.getAsJsonObject("response");
            JsonArray jsonArray = jsonObject.getAsJsonArray("venues");

            for (int i = 0; i < jsonArray.size(); i++)
            {
                JsonObject jsonPlaceObject = (JsonObject) jsonArray.get(i);
                JsonObject jsonStatsObject = jsonPlaceObject.getAsJsonObject("stats");
                Place place = new Place(jsonPlaceObject.get("id").getAsString(), jsonPlaceObject.get("name").getAsString(), jsonStatsObject.get("checkinsCount").getAsString());

                places.add(place);
            }

            view.placeAdapterNotifyDataSetChanged();
            view.dismissProgressDialog();
        }

        @Override
        public void onCompleted()
        {
            for(Place place : places)
            {
                loadPhotosForPlace(place);
            }
        }

        @Override
        public void onError(Throwable e)
        {
            ErrorHandler.getInstance().showErrorToast(view, (Exception) e);
            e.printStackTrace();
        }
    };

    private void loadPlaces()
    {
        placeService.getApi().getPlaces(getLLAsStringForUrl(), PLACES_LIMIT, CLIENT_ID, SECRET_ID, CurrentDate.getAsString())
            .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(loadPlacesSubscriber);
    }

    private void downloadPhoto(final Place place)
    {
        Observable.create(new Observable.OnSubscribe<Place>()
        {
            @Override
            public void call(Subscriber<? super Place> subscriber)
            {
                InputStream in = null;
                try
                {
                    in = new java.net.URL(place.getPhotoUrl()).openStream();
                    place.setPhoto(BitmapFactory.decodeStream(in));
                }
                catch (IOException e)
                {
                    subscriber.onError(e);
                }
                finally
                {
                    subscriber.onNext(place);
                }
            }
        })
            .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
            .subscribe(new Subscriber<Place>()
            {
                @Override
                public void onCompleted()
                {

                }

                @Override
                public void onError(Throwable e)
                {
                    ErrorHandler.getInstance().showErrorToast(view, (Exception) e);
                    e.printStackTrace();
                }

                @Override
                public void onNext(Place place)
                {
                    view.placeAdapterNotifyItemChanged(places.indexOf(place));
                }
            });
    }

    private void loadPhotosForPlace(final Place place)
    {
        placeService.getApi().getPhotos(place.getId(), PHOTO_LIMIT, CLIENT_ID, SECRET_ID, CurrentDate.getAsString())
                .subscribeOn(Schedulers.newThread()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<JsonElement>()
                {
                    @Override
                    public void onCompleted()
                    {

                    }

                    @Override
                    public void onError(Throwable e)
                    {
                        ErrorHandler.getInstance().showErrorToast(view, (Exception) e);
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(JsonElement jsonElement)
                    {
                        JsonObject jsonObject = (JsonObject) jsonElement;
                        jsonObject = jsonObject.getAsJsonObject("response");
                        jsonObject = jsonObject.getAsJsonObject("photos");
                        int count = jsonObject.get("count").getAsInt();

                        if (count >= 1)
                        {
                            JsonArray jsonPhotosArray = jsonObject.getAsJsonArray("items");

                            JsonObject jo = (JsonObject) jsonPhotosArray.get(0);
                            place.setPhotoUrl(jo.get("prefix").getAsString(), jo.get("suffix").getAsString());

                            if (place.getPhotoUrl() == null || place.getPhoto() != null)
                            {
                                return;
                            }

                            downloadPhoto(place);
                        }
                    }
                });
    }
}
