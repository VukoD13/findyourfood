package com.onemanarmy.vuko.findyourfood.activities.home;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.onemanarmy.vuko.findyourfood.R;

/**
 *
 */
public class PlaceViewHolder extends RecyclerView.ViewHolder
{
    public TextView nameTextView;
    public TextView checkingsTextView;
    public ImageView placePhotoImageView;

    public PlaceViewHolder(View view)
    {
        super(view);
        nameTextView = (TextView) view.findViewById(R.id.nameTextView);
        checkingsTextView = (TextView) view.findViewById(R.id.checkinsTextView);
        placePhotoImageView = (ImageView) view.findViewById(R.id.placePhotoImageView);
    }
}