package com.onemanarmy.vuko.findyourfood.utility;

import android.content.Context;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;

import com.onemanarmy.vuko.findyourfood.activities.first.FirstActivity;
import com.onemanarmy.vuko.findyourfood.activities.first.FirstActivityPresenter;
import com.onemanarmy.vuko.findyourfood.activities.home.HomeActivityPresenter;

/**
 *
 */
public class Tracker implements LocationListener
{
    private FirstActivityPresenter firstActivityPresenter;
    private HomeActivityPresenter homeActivityPresenter;

    private static Tracker instance = null;
    private final int MIN_DISTANCE_CHANGE_FOR_UPDATES = 100; // w metrach
    private final int MIN_TIME_BETWEEN_UPDATES = 1000 * 10; // w milisekundach //mala wartosc do testow

    private Location lastKnownLocation;
    private LocationManager locationManager;

    private double latitude, longitude;

    public static Tracker getInstance()
    {
        if (instance == null)
        {
            instance = new Tracker();
        }
        return instance;
    }

    public void setHomeActivityPresenter(HomeActivityPresenter homeActivityPresenter)
    {
        this.homeActivityPresenter = homeActivityPresenter;
    }

    public void initialize(FirstActivityPresenter firstActivityPresenter)
    {
        this.firstActivityPresenter = firstActivityPresenter;
        FirstActivity view = firstActivityPresenter.getView();

        locationManager = (LocationManager) view.getSystemService(Context.LOCATION_SERVICE);

        Criteria criteria = new Criteria();
        String locationProvider = locationManager.getBestProvider(criteria, false);
        lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

        locationManager.requestLocationUpdates(locationProvider, MIN_TIME_BETWEEN_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES, this);


        if (locationManager != null)
        {
            lastKnownLocation = locationManager.getLastKnownLocation(locationProvider);

            if (lastKnownLocation != null)
            {
                latitude = lastKnownLocation.getLatitude();
                longitude = lastKnownLocation.getLongitude();

                /*nie ma potrzeby brac ostatniej lokacji przy tej szybkosci dzialania apki
                if (firstActivityPresenter.isFirstActivityActive())
                {
                    firstActivityPresenter.startHomeActivity();
                }*/
            }
        }
    }

    public boolean isGpsEnabled()
    {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }

    @Override
    public void onLocationChanged(Location location)
    {
        if (location != null)
        {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            if (firstActivityPresenter.isFirstActivityActive())
            {
                firstActivityPresenter.startHomeActivity();
            }
            else
            {
                if(homeActivityPresenter != null)
                {
                    homeActivityPresenter.recreateHomeActivity();
                }
            }
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {

    }

    @Override
    public void onProviderEnabled(String provider)
    {

    }

    @Override
    public void onProviderDisabled(String provider)
    {

    }

    public double getLatitude()
    {
        return latitude;
    }

    public double getLongitude()
    {
        return longitude;
    }
}
