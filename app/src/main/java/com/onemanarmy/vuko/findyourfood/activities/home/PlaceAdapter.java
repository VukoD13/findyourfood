package com.onemanarmy.vuko.findyourfood.activities.home;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.onemanarmy.vuko.findyourfood.R;
import com.onemanarmy.vuko.findyourfood.model.domain.Place;

import java.util.List;

/**
 *
 */
public class PlaceAdapter extends RecyclerView.Adapter<PlaceViewHolder>
{
    private List<Place> places;

    public PlaceAdapter(List<Place> places)
    {
        this.places = places;
    }

    @Override
    public int getItemCount()
    {
        return places.size();
    }

    @Override
    public PlaceViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View itemView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.activity_home_card_view, parent, false);

        return new PlaceViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(PlaceViewHolder placeViewHolder, int position)
    {
        Place place = places.get(position);
        placeViewHolder.nameTextView.setText(place.getName());
        placeViewHolder.checkingsTextView.setText(place.getStats().getCheckinsCount());

        if (place.getPhoto() != null)
        {
            placeViewHolder.placePhotoImageView.setImageBitmap(place.getPhoto());
        }
    }
}
