package com.onemanarmy.vuko.findyourfood.utility;

import android.app.Activity;
import android.widget.Toast;

/**
 *
 */
public class ErrorHandler
{
    private static ErrorHandler instance = null;

    public static ErrorHandler getInstance()
    {
        if (instance == null)
        {
            instance = new ErrorHandler();
        }
        return instance;
    }

    public void showErrorToast(final Activity view, final Exception e)
    {
        view.runOnUiThread(new Runnable()
        {
            public void run()
            {
                Toast.makeText(view.getApplication(), e.getLocalizedMessage() + "\n" + view.toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
