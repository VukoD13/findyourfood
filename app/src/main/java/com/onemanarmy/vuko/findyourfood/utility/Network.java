package com.onemanarmy.vuko.findyourfood.utility;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 *
 */
public class Network
{
    private static Network instance = null;

    public static Network getInstance()
    {
        if (instance == null)
        {
            instance = new Network();
        }
        return instance;
    }

    public boolean isNetworkEnabled(Activity activity)
    {
        ConnectivityManager manager = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();

        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected())
        {
            isAvailable = true;
        }
        return isAvailable;
    }
}
