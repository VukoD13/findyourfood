package com.onemanarmy.vuko.findyourfood.model.service;

import com.google.gson.JsonElement;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;
import rx.Observable;

/**
 *
 */
public class PlaceService
{
    private static final String BASE_URL = "https://api.foursquare.com/v2/venues";
    private PlaceApi placeApi;

    public PlaceService()
    {
        RequestInterceptor requestInterceptor = new RequestInterceptor()
        {
            @Override
            public void intercept(RequestFacade request)
            {
                request.addHeader("Accept", "application/json");
            }
        };

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();

        placeApi = restAdapter.create(PlaceApi.class);
    }

    public interface PlaceApi
    {
        @GET( "/search" )
        Observable<JsonElement> getPlaces
                (
                        @Query("ll") String position,
                        @Query("limit") int limit,
                        @Query("client_id") String clientId,
                        @Query("client_secret") String secretId,
                        @Query("v") String currentDate
                );

        @GET( "/{id}/photos" )
        Observable<JsonElement> getPhotos
                (
                        @Path("id") String id,
                        @Query("limit") int limit,
                        @Query("client_id") String clientId,
                        @Query("client_secret") String secretId,
                        @Query("v") String currentDate
                );
    }

    public PlaceApi getApi()
    {
        return placeApi;
    }
}
