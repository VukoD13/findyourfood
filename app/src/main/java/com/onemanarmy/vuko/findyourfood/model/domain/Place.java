package com.onemanarmy.vuko.findyourfood.model.domain;

import android.graphics.Bitmap;

/**
 *
 */
public class Place
{
    private static final int PHOTO_SIZE = 48;
    private String id;
    private String name;
    private String photoUrl = null;
    private Bitmap photo = null;
    private Stats stats;

    public Place(String id, String name, String checkinsCount)
    {
        this.id = id;
        this.name = name;

        stats = new Stats(checkinsCount);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public Stats getStats()
    {
        return stats;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getPhotoUrl()
    {
        return photoUrl;
    }

    public void setPhotoUrl(String prefix, String suffix)
    {
        String str = prefix + PHOTO_SIZE + suffix;
        this.photoUrl = str;
    }

    public Bitmap getPhoto()
    {
        return photo;
    }

    public void setPhoto(Bitmap photo)
    {
        this.photo = photo;
    }

    public class Stats
    {
        private String checkinsCount;

        public Stats(String checkinsCount)
        {
            this.checkinsCount = checkinsCount;
        }

        public String getCheckinsCount()
        {
            return checkinsCount;
        }
    }
}
