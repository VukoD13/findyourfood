package com.onemanarmy.vuko.findyourfood.activities.first;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.onemanarmy.vuko.findyourfood.R;

public class FirstActivity extends AppCompatActivity
{
    private FirstActivityPresenter activityPresenter;

    private TextView firstTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        viewInit();

        activityPresenter = new FirstActivityPresenter(this);
        activityPresenter.onCreate();
    }

    private void viewInit()
    {
        firstTextView = (TextView) findViewById(R.id.firstTextView);
    }

    public void showError()
    {
        firstTextView.setText(R.string.errorTextView);
    }

    @Override
    public String toString()
    {
        return "FirstActivity";
    }
}
