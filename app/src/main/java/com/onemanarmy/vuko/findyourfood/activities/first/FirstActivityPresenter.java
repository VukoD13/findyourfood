package com.onemanarmy.vuko.findyourfood.activities.first;

import android.content.Intent;

import com.onemanarmy.vuko.findyourfood.activities.IActivityPresenter;
import com.onemanarmy.vuko.findyourfood.activities.home.HomeActivity;
import com.onemanarmy.vuko.findyourfood.utility.Network;
import com.onemanarmy.vuko.findyourfood.utility.Tracker;

/**
 *
 */
public class FirstActivityPresenter implements IActivityPresenter
{
    private FirstActivity view;

    private boolean isFirstActivityActive;

    public FirstActivityPresenter(FirstActivity view)
    {
        this.view = view;
    }

    public void startHomeActivity()
    {
        isFirstActivityActive = false;

        view.finish();
        Intent intent = new Intent(view.getApplicationContext(), HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        view.startActivity(intent);
    }

    @Override
    public void onCreate()
    {
        isFirstActivityActive = true;

        Tracker.getInstance().initialize(this);

        if(!isNetworkEnabled() || !isGpsEnabled())
        {
            onNetworkOrGpsUnavailable();
        }
    }

    private boolean isNetworkEnabled()
    {
        return Network.getInstance().isNetworkEnabled(view);
    }

    private boolean isGpsEnabled()
    {
        return Tracker.getInstance().isGpsEnabled();
    }

    private void onNetworkOrGpsUnavailable()
    {
        view.showError();
    }

    public FirstActivity getView()
    {
        return view;
    }

    public boolean isFirstActivityActive()
    {
        return isFirstActivityActive;
    }
}
