package com.onemanarmy.vuko.findyourfood.activities;

/**
 *
 */
public interface IActivityPresenter
{
    public void onCreate();
}
