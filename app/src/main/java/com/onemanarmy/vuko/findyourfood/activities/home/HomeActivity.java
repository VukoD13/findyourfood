package com.onemanarmy.vuko.findyourfood.activities.home;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.onemanarmy.vuko.findyourfood.R;
import com.onemanarmy.vuko.findyourfood.model.domain.Place;

import java.util.List;

public class HomeActivity extends AppCompatActivity
{
    private HomeActivityPresenter activityPresenter;

    private PlaceAdapter placeAdapter;

    private RecyclerView recyclerView;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        viewInit();

        activityPresenter = new HomeActivityPresenter(this);
        activityPresenter.onCreate();
    }

    public void setRecyclerView(List<Place> places)
    {
        placeAdapter = new PlaceAdapter(places);
        recyclerView.setAdapter(placeAdapter);
    }

    private void viewInit()
    {
        recyclerView = (RecyclerView) findViewById(R.id.cardList);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
    }

    public void placeAdapterNotifyItemChanged(int position)
    {
        placeAdapter.notifyItemChanged(position);
    }

    public void placeAdapterNotifyDataSetChanged()
    {
        placeAdapter.notifyDataSetChanged();
    }

    public void showProgressDialog()
    {
        progressDialog = ProgressDialog.show(HomeActivity.this, getString(R.string.loadPlacesTitleStatement), getString(R.string.loadPlacesMessageStatement), true, true);
    }

    public void dismissProgressDialog()
    {
        progressDialog.dismiss();
    }

    @Override
    public String toString()
    {
        return "HomeActivity";
    }
}
